use regex::Regex;
use structopt::StructOpt;

fn main() {
    let opts = Opt::from_args();
    let id = extract_id(&opts).expect("unable to extract id");
    println!("open {:?}", id.pdf_url());
    std::process::Command::new("okular")
        .arg(id.pdf_url())
        .status()
        .unwrap();
}

#[derive(Debug, StructOpt)]
#[structopt(name = "arxiv-open")]
pub struct Opt {
    pub url: String,
}

#[derive(Debug, Clone)]
pub enum Prefix {
    New {
        date: String,
    },
    Old {
        archive: String,
        subject_class: Option<String>,
    },
}

#[derive(Debug, Clone)]
pub struct Identifier {
    prefix: Prefix,
    num: String,
    version: Option<usize>,
}

impl Identifier {
    pub fn display(&self) -> String {
        match self {
            Identifier {
                prefix: Prefix::New { date },
                num,
                version: Some(version),
            } => format!("{}.{}v{}", date, num, version),

            Identifier {
                prefix: Prefix::New { date },
                num,
                version: None,
            } => format!("{}.{}", date, num),
            Identifier {
                prefix:
                    Prefix::Old {
                        archive,
                        subject_class,
                    },
                num,
                version: Some(version),
            } => format!(
                "{}{}/{}v{}",
                archive,
                subject_class
                    .clone()
                    .map(|s| format!(".{}", &s))
                    .unwrap_or_else(String::new),
                num,
                version
            ),
            Identifier {
                prefix:
                    Prefix::Old {
                        archive,
                        subject_class,
                    },
                num,
                version: None,
            } => format!(
                "{}{}/{}",
                archive,
                subject_class
                    .clone()
                    .map(|s| format!(".{}", &s))
                    .unwrap_or_else(String::new),
                num
            ),
        }
    }

    pub fn pdf_url(&self) -> String {
        format!("https://arxiv.org/pdf/{}", self.display())
    }
}

/// https://arxiv.org/help/arxiv_identifier
fn extract_id(opt: &Opt) -> Option<Identifier> {
    let re_url = Regex::new(
        r"(?x)
((http|https|arxiv|arXiv)://)?
(www\.)?
arxiv.org
(/abs/|/pdf/)
((                    # post-2007 identifier
(?P<date>\d{4})
\.
(?P<newnum>\d+)
)|(                   # pre-2007 identifier
(?P<archive>[a-z-]+)
(\.                   # subject class
(?P<subjcls>[A-Z]+)
)?
/
(?P<num>\d+)
))
(v(?P<version>\d+))?  # version
((                    # ignore url params
/.*
)|(
\?.*
))?
",
    )
    .unwrap();

    let re_vanilla = Regex::new(
        r"(?x)
(arXiv:|arxiv:)?
((                    # post-2007 identifier
(?P<date>\d{4})
\.
(?P<newnum>\d+)
)|(                   # pre-2007 identifier
(?P<archive>[a-z-]+)
(\.                   # subject class
(?P<subjcls>[A-Z]+)
)?
/
(?P<num>\d+)
))
(v(?P<version>\d+))?  # version
",
    )
    .unwrap();

    let caps = re_url
        .captures(&opt.url)
        .or_else(|| re_vanilla.captures(&opt.url))
        .unwrap();
    if let (Some(date), Some(newnum)) = (caps.name("date"), caps.name("newnum")) {
        Some(Identifier {
            prefix: Prefix::New {
                date: date.as_str().to_owned(),
            },
            num: newnum.as_str().to_owned(),
            version: caps.name("version").and_then(|m| m.as_str().parse().ok()),
        })
    } else if let (Some(archive), Some(num)) = (caps.name("archive"), caps.name("num")) {
        Some(Identifier {
            prefix: Prefix::Old {
                archive: archive.as_str().to_owned(),
                subject_class: caps.name("subjcls").map(|m| m.as_str().to_owned()),
            },
            num: num.as_str().to_owned(),
            version: caps.name("version").and_then(|m| m.as_str().parse().ok()),
        })
    } else {
        None
    }
}
