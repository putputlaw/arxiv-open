arxiv-open
==========

```
arxiv-open [URL]
```


A command-line utility which opens arXiv papers given a URL.

It accepts as URL
- https://arxiv.org/abs/1611.09010
- http://arxiv.org/pdf/1611.09010
- 1611.09010
- arxiv:1611.09010
- arXiv:1611.09010
- pre-2007 papers
